# function to compare the timestamp of a file (tagfile) to the date last modification of another file in git (repofile)

import git
import os.path, datetime, sys
import pytz

# the function has to be executed in the root of the repository and
# repofile path should be relative to that root
# return true if repofile is younger than tagfile (or tagfile does not exist)
# else return false
def updatedRepo(repofile, tagfile) :
    # get last time of modification
    r = git.Repo('.')
    ctrepo=r.git.log('-1','--format=%cd',repofile)
    try : 
        trepo=datetime.datetime.strptime(ctrepo, "%a %b %d %H:%M:%S %Y %z")
    except ValueError :
        print("Problems decoding the time from the repository. Did you commit all files?",file=sys.stderr)
        exit()
    
    # convert to utc
    utc = pytz.utc
    trepoutc=trepo.astimezone(utc)
    print(trepoutc,file=sys.stderr)
    
    # check if tagfile exists
    # get timestamp and make it timezone aware
    try :
        ttag=datetime.datetime.utcfromtimestamp(os.path.getmtime(tagfile)).replace(tzinfo=utc)
        print(ttag,file=sys.stderr)
    except FileNotFoundError :
        print("{} not found".format(tagfile),file=sys.stderr)
        # in this case the repofile is younger
        return True

    timedif=ttag-trepo
    print(timedif,file=sys.stderr)

    if trepo>ttag :
        print("Repofile younger than tagfile",file=sys.stderr)
        return True;
    else : return False
