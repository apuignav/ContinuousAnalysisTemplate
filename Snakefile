##############################################
# a simple analysis pipline
#  

##############################################
# Preamble, configuration and helpers 

# The config file sets the working directory, 
# you can use it to set other global variables
# you can overwrite this and specify your own configfile on the command line
# with snakemake -c <myconfigfile>
configfile: "config/default.yml"
import os
os.environ["WORKDIR"] = config["workdir"]
work_dir=config["workdir"]

# tool to dynamically generate dependencies based on when a file was last modified on git
include: "gitcheck.snake"
# usage ::: 
# input : gitchecked([listofinput],[listofcode],[listofoutput])

###############################################

include: "cmake.snake"

###############################################
# The rules to run the analysis start here

# the toplevel rule
rule analysis:
     input : work_dir+"/fitresult.root", "latex/CAT.pdf"

rule maketoy:
     input : gitchecked([],["scripts/toys/generateToy.C"],[work_dir+"/toy.root"])
     output : work_dir+"/toy.root"
     shell: 'mkdir -p $WORKDIR; root -b -q scripts/toys/generateToy.C\(\\"$WORKDIR/toy.root\\"\);'

	
# use beef to make fit. first create a roofit workspace
rule workspace:
     input : work_dir+"/toy.root",
     	     "./build/bin/createWorkspace",
	     "config/wsconfig.info",
     output : work_dir+"/ws.root"
     shell : "./build/bin/createWorkspace -c config/wsconfig.info -t toy -i toy.root -o ws.root -d $WORKDIR"


# now do the fit
rule fit:
     input :  work_dir+"/ws.root",
     	      "./build/bin/beef",
	      "config/fitconfig.info"
     output : work_dir+"/fitresult.root",
     	      work_dir+"/Xfit.png"
     shell : "./build/bin/beef -c config/fitconfig.info -i ws.root -o fitresult.root -d $WORKDIR"


# build some slides
rule slides:
     input : work_dir+"/Xfit.png",
     	     "latex/CAT.tex",
	     "latex/signatures.tex"
     output : "latex/CAT.pdf"
     shell : "cp $WORKDIR/Xfit.png latex/pics; cd latex; lualatex CAT.tex"


# end of analysis
###############################################