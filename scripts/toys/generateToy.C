// simple script to generate pseudo-data

#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include "TRandom3.h"

void
generateToy(TString outfilename="toy.root",
            unsigned int nevents=10000)
{
  
  TFile* outfile=TFile::Open(outfilename,"RECREATE");
  TTree* outtree=new TTree("toy","toy");
  
  // variables
  double x,y;  
  double R;
  
  outtree->Branch("x",&x,"x/D");
  outtree->Branch("y",&y,"y/D");
  //outtree->Branch("R",&R,"R/D");
  
  // event loop
  for(unsigned int i=0; i<nevents; ++i)
  {
    R=gRandom->Uniform();
    if(R>0.7)
    {
      x=gRandom->Gaus(5,0.5);
    }
    else 
    {
      x=gRandom->Exp(2);
    }
    
    y=gRandom->Gaus(-1.+2.*R,1);
    
    outtree->Fill();
    
  }
  
  outtree->Write();
  outfile->Close();
  

}



