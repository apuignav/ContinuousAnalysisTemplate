# Example analysis pipeline
Here is an explanation of the example pipeline and how it is implemented in the `Snakefile`.
Please also have a look at the [snakemake documentation](https://bitbucket.org/snakemake/snakemake/wiki/Documentation) and their excellent [tutorial](http://snakemake.bitbucket.org/snakemake-tutorial.html)

For usage on the CERN gitlab, we have added a few helpers. The main goal here is to really only rerun those parts of your analysis which are necessary to redo on a change.

Let's look at the [Snakefile](Snakefile)
```
##############################################
# a simple analysis pipline
#  

##############################################
# Preamble, configuration and helpers 

# The config file sets the working directory, 
# you can use it to set other global variables
# you can overwrite this and specify your own configfile on the command line
# with snakemake -c <myconfigfile>
configfile: "config/default.yml"
import os
os.environ["WORKDIR"] = config["workdir"]
work_dir=config["workdir"]

# tool to dynamically generate dependencies based on when a file was last modified on git
include: "gitcheck.snake"
# usage ::: 
# input : gitchecked([listofinput],[listofcode],[listofoutput])

###############################################
# The rules to run the analysis start here

# the toplevel rule
rule analysis:
     input : work_dir+"/fitresult.root"

rule maketoy:
     input : gitchecked([],["scripts/toys/generateToy.C"],[work_dir+"/toy.root"])
     output : work_dir+"/toy.root"
     shell: 'mkdir -p $WORKDIR; root -b -q scripts/toys/generateToy.C\(\\"$WORKDIR/toy.root\\"\);'

	
# use beef to make fit. first create a roofit workspace
rule workspace:
     input : gitchecked([work_dir+"/toy.root"],["config/wsconfig.info"],[work_dir+"/ws.root"])
     output : work_dir+"/ws.root"
     shell : "createWorkspace -c config/wsconfig.info -t toy -i toy.root -o ws.root -d $WORKDIR"


# now do the fit
rule fit:
     input :  gitchecked([work_dir+"/ws.root"],["config/fitconfig.info"],[work_dir+"/fitresult.root"])
     output : work_dir+"/fitresult.root"
     shell : "beef -c config/fitconfig.info -i ws.root -o fitresult.root -d $WORKDIR"

# end of analysis
###############################################
```