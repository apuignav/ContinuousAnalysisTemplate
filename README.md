# Continuous Analysis Template
A template to setup a continuous analysis system, including an automatic [data processing pipeline](https://en.wikipedia.org/wiki/Pipeline_(computing)) and a [continuous integration](https://en.wikipedia.org/wiki/Continuous_integration) server. 
This to test the analysis after every change or addition of a feature, ensuring maximally consistent results.

This README contains all the information to get the data analysis pipeline running.

Table of Contents: 
* [Installation of local workspace](https://gitlab.cern.ch/sneubert/ContinuousAnalysisTemplate#installation-of-local-workspace)
* [Quick-Start: Running the example analysis locally](https://gitlab.cern.ch/sneubert/ContinuousAnalysisTemplate#quick-start-running-the-example-analysis-locally)

There is further documentation available on these topics:
* [How to run the pipeline in continuous integration](CI.md)
* How to use the template to setup your own analysis
* Working in a team

Additions to this template are very welcome! Please have a look at how you can [contribute](CONTRIBUTING.md).

Powered by [![Snakemake](https://img.shields.io/badge/snakemake-≥3.5.2-brightgreen.svg?style=flat-square)](http://snakemake.bitbucket.org)
This template uses the [gitlab-ci](http://doc.gitlab.com/ce/ci/) to run the [ANA-Docker](https://gitlab.cern.ch/sneubert/ANA-Docker/) container on the [CERN cloud infrastructure](https://clouddocs.web.cern.ch/clouddocs/).

# Installation of local workspace
You will need a few tools to setup a flexible work environment that will allow you to easily manage all the dependencies of your project. We use conda for that.


## Install the conda package manager 
Download and install `miniconda` with python2.7 as described [here](http://conda.pydata.org/docs/install/quick.html). For example:
```
 wget https://repo.continuum.io/miniconda/Miniconda-latest-Linux-x86_64.sh
 chmod +x Miniconda-latest-Linux-x86_64.sh
 ./Miniconda-latest-Linux-x86_64.sh
```

Test your installation:
```
conda --version
conda update conda
```

## Make a conda environment with snakemake
Snakemake is a data analysis pipeline manager. We get it from the bioconda stream. It needs python3. We also need a few useful Python libraries. 

Create a conda environment called `snake` to hold the installation. And install several further useful libraries and tools
```
conda create -n snake -c bioconda python=3.4 snakemake beautiful-soup pyyaml
conda install -n snake -c https://conda.anaconda.org/conda-forge gitpython
conda install -n snake -c https://conda.anaconda.org/anaconda pytz
```

## ROOT
This template assumes you have installed ROOT v6.
On lxplus you can simply do
```
SetupProject ROOT
```

# Quick-Start: Running the example analysis locally
Make sure you have installed the prerequisites described above.

Clone the analysis repository 
```
git clone --recursive <repository_url>
```

Activate the conda environment for the analysis:
```
source activate snake
```
Go into the analysis directory and run the analysis by doing
```
cd <path_to_ana>
snakemake
```
Have a look at the analysis pipeline defined in the [Snakefile](Snakefile) and look at the [explanations](EXAMPLE.md).

Inspect the output the pipeline produced
```
ls /tmp/anatemplate
```

To get all the available rules:
```
snakemake --list
analysis
maketoy
workspace
fit
```

If you want to recalculate just one step do
```
snakemake <rulename>
```
If you want to recalculate a step and all its dependencies do
```
snakemake -R <rulename>
```

